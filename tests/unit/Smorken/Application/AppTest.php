<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 7:22 AM
 */

use Smorken\Application\App;
use Mockery as m;

class ServiceStub extends \Smorken\Service\Service {

    public function start()
    {
        $this->name = 'service.test';
    }

}
class DeferredServiceStub extends ServiceStub {

    protected $deferred = true;

    public function start()
    {
        $this->name = 'service.test';
    }
}


class AppTest extends \PHPUnit_Framework_TestCase {

    public function tearDown()
    {
        m::close();
    }

    public function testContainerSetAndGet()
    {
        $c = m::mock('Pimple\Container');
        $c->shouldReceive('offsetSet')
            ->once()
            ->with('foo', 'bar');
        $c->shouldReceive('offsetGet')
            ->once()
            ->with('foo')
            ->andReturn('bar');
        App::getInstance($c)['foo'] = 'bar';
        $this->assertEquals('bar', App::getInstance()['foo']);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testContainerUnsetThrowsException()
    {
        $c = m::mock('Pimple\Container');
        $c->shouldReceive('offsetGet')
            ->once()
            ->with('bar')
            ->andThrow('\InvalidArgumentException');
        App::getInstance($c)['bar'];
    }

    public function testServiceRegisterNotDeferred()
    {
        $c = m::mock('Pimple\Container');
        $c->shouldReceive('offsetExists')
            ->with('service.test')->andReturn(false);
        $c->shouldReceive('offsetSet')
            ->with('service.test', m::type('stdClass'));
        $app = App::getInstance($c);
        $s = $app->registerService('ServiceStub');
        $this->assertInstanceOf('Smorken\Service\ServiceInterface', $s);
    }

    public function testServiceRegisterDeferred()
    {
        $c = m::mock('Pimple\Container');
        $c->shouldReceive('offsetSet')
            ->never();
        $app = App::getInstance($c);
        $s = $app->registerService('DeferredServiceStub');
        $this->assertInstanceOf('Smorken\Service\ServiceInterface', $s);
    }

    public function testContainerGetsServiceLoaded()
    {
        $c = m::mock('Pimple\Container');
        $c->shouldReceive('offsetExists')
            ->with('service.test')->andReturn(false, true);
        $c->shouldReceive('offsetSet')
            ->with('service.test', m::type('stdClass'));
        $c->shouldReceive('offsetGet')
            ->with('service.test')
            ->andReturn(new stdClass());
        $app = App::getInstance($c);
        $app->registerService('ServiceStub');
        $o = $app['service.test'];
        $this->assertInstanceOf('stdClass', $o);
    }

    public function testContainerGetsDeferredServiceLoaded()
    {
        $error = new \InvalidArgumentException("service.test");
        $c = m::mock('Pimple\Container');
        $c->shouldReceive('offsetGet')
            ->once()
            ->with('service.test')
            ->andThrow($error);
        $c->shouldReceive('offsetSet')
            ->with('service.test', m::type('stdClass'));
        $c->shouldReceive('offsetGet')
            ->once()
            ->with('service.test')
            ->andReturn(new stdClass());
        $app = App::getInstance($c);
        $app->registerService('DeferredServiceStub');
        $o = $app['service.test'];
        $this->assertInstanceOf('stdClass', $o);
    }
} 