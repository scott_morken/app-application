This project borrows heavily from the excellent [Laravel 4](http://laravel.com) and [Symfony 2](http://symfony.com) projects.

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

~~~~
<?php
include 'vendors/autoload.php';
$container = new Pimple\Container();
$app = Smorken\Application\App::getInstance($container);

$app->addPaths(require __DIR__ . '/paths.php');
$app->start();

define('DEBUG', true);

return $app;
~~~~
