<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 7:16 AM
 */

namespace Smorken\Application;

use Pimple\Container;

/**
 * Class App
 * @package Smorken\Application
 *
 * This ties together the services with the dependency injection container (Pimple)
 * It is a singleton *gasp*
 */
class App implements \ArrayAccess {

    /**
     * @var Container
     */
    private $container;
    /**
     * @var \Smorken\Service\Service[]
     */
    private $services = array();

    /**
     * @var App
     */
    private static $instance;

    private function __construct($container)
    {
        include_once __DIR__ . '/helpers.php';
        $this->setContainer($container);
    }

    /**
     * The first intantiation requires the container be passed in
     * Reinstantiating does not, unless you want a new app
     * @param null|Container $container
     * @return App
     */
    public static function getInstance($container = null)
    {
        if ( !isset(self::$instance) || $container instanceof Container)
        {
            self::$instance = new self($container);

        }

        return self::$instance;
    }

    /**
     * Destroys the instance of app
     */
    public static function destroy()
    {
        self::$instance = null;
    }

    /**
     * "Makes" $name by pulling the closure from the service
     * @param $name
     * @return mixed
     */
    public static function make($name)
    {
        $app = self::$instance;
        return $app[$name];
    }

    /**
     * Registers the services
     */
    public function start()
    {
        $this->registerCoreServices();
        $this->registerAppServices();
    }

    /**
     * Sets the DI Container
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Registers any additional services provided by config/app - services
     */
    protected function registerAppServices()
    {
        $services = $this['config']->get('app.services', array());
        foreach($services as $service) {
            $this->registerService($service);
        }
    }

    /**
     * Registers the core services
     */
    protected function registerCoreServices()
    {
        $services = $this->getCoreServices();
        $this->registerServices($services);
    }

    /**
     * Returns an array of the core services that are always loaded
     * @return array
     */
    protected function getCoreServices()
    {
        return array(
            'Smorken\Config\ConfigService',
            'Smorken\Logger\LoggerService',
            'Smorken\Error\ErrorService',
            'Smorken\Session\SessionService',
            'Smorken\Http\UrlService',
        );
    }

    /**
     * Returns all the services
     * @return \Smorken\Service\Service[]
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Registers an array of services
     * @param $services
     */
    public function registerServices($services)
    {
        foreach($services as $servicecls) {
            $this->registerService($servicecls);
        }
    }

    /**
     * Registers a service via its ServiceProvider
     * Starts the service and if it is not deferred, loads it and
     * runs its providers
     * @param $servicecls
     * @return \Smorken\Service\ServiceInterface
     */
    public function registerService($servicecls)
    {
        $s = new $servicecls($this);
        $s->start();
        $name = $s->getName();
        $this->services[$name] = $s;
        if (!$s->isDeferred()) {
            $s->load();
            $this->runServiceProvides($s, $name);

        }
        return $s;
    }

    /**
     * Runs any provides from a service provider
     * @param $s
     * @param $name
     */
    protected function runServiceProvides($s, $name)
    {
        if ($s->provides()) {
            $p = (array) $s->provides();
            foreach ($p as $k) {
                $this->runService($k);
            }
        }
        else {
            $this->runService($name);
        }
    }

    /**
     * Runs the callable for the service name
     * @param $name
     */
    protected function runService($name)
    {
        if (isset($this[$name]) && is_callable($this[$name])) {
            $this[$name];
        }
    }

    /**
     * Adds path information to the DI container
     * @param $paths
     */
    public function addPaths($paths)
    {
        foreach($paths as $pathname => $path) {
            $this['path.' . $pathname] = realpath($path);
        }
    }

    /**
     * Adds an instance (single) of $closure to the DI container
     * @param $name
     * @param $closure
     */
    public function instance($name, $closure) {
        $this->container[$name] = $closure;
    }

    /**
     * Binds $closure to $name in the DI container, multiple calls will result in
     * a new object being created
     * @param $name
     * @param $closure
     */
    public function bind($name, $closure) {
        $this->container[$name] = $this->container->factory($closure);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return $this->container->offsetExists($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @throws \Exception
     * @throws \InvalidArgumentException
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        try {
            return $this->container->offsetGet($offset);
        }
        catch (\InvalidArgumentException $e) {
            if (isset($this->services[$offset])) {
                $this->services[$offset]->load();
                return $this->container[$offset];
            }
            throw $e;
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->container->offsetSet($offset, $value);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        $this->container->offsetUnset($offset);
    }
}