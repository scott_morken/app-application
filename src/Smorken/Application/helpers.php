<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/28/15
 * Time: 7:42 AM
 */
if (!function_exists('app')) {
    /**
     * @param null|string $lookup
     * @param null|Pimple\Container $container
     * @return mixed|\Smorken\Application\App
     */
    function app($lookup = null, $container = null)
    {
        $app = Smorken\Application\App::getInstance($container);
        if ($lookup) {
            return $app[$lookup];
        }
        return $app;
    }
}